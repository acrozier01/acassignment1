Tomcat 8.575 must be installed on your machine for the application to work.

After Tomcat is installed, download the project and open it in JetBrains IntelliJ IDEA 2021.3.1.

I chose the MIT license because the project is just a tiny thing done for a small school assignment. I dont expect many people to want to use this code.

This change will be reverted.